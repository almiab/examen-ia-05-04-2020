; ------------------------------------------------------------------------------------
; Examen - Inteligencia Artificial
; Autor: Alejandro Mira Abad
; Anotaciones:
; No me da tiempo a acabar, 
: si hago retract cuando muevo el robot
; y cuando no lo hago termina sin entregar todas las cargas.
; ------------------------------------------------------------------------------------

; ------------------------------------------------------------------------------------
; Variables globales
; ------------------------------------------------------------------------------------
(defglobal
    ?*cargaMax* = 8
    ?*posMax* = 5
    ?*posMin* = 1

)
; ------------------------------------------------------------------------------------

; ------------------------------------------------------------------------------------
; Base de hechos
; ------------------------------------------------------------------------------------
(deffacts datos
    (robot boxAcual 1 piezas 0)
    (box 1 piezas 0 destino 0 entregadas 0)
    (box 2 piezas 2 destino 1 entregadas 0)
    (box 3 piezas 8 destino 4 entregadas 0)
    (box 4 piezas 4 destino 3 entregadas 0)
    (box 5 piezas 0 destino 0 entregadas 0)
)

; ------------------------------------------------------------------------------------

; ------------------------------------------------------------------------------------
; Reglas
; ------------------------------------------------------------------------------------

(defrule cogerCarga
    (declare (salience 350))
    ?r <- (robot boxAcual ?x piezas ?pR)
    ?b <- (box ?x piezas ?pB destino ?d entregadas ?e)
    (test (> ?d 0))
    (test (< ?pR ?*cargaMax*))
    (test (> ?pB 0))
=>
    (retract ?r)
    (retract ?b)
    (assert (robot boxAcual ?x piezas (+ ?pR 1)))
    (assert (box ?x piezas (- ?pB 1) destino ?d entregadas ?e))
    (assert (pieza origen ?x destino ?d))
)

(defrule dejarCarga
    (declare (salience 300))
    ?r <- (robot boxAcual ?x piezas ?pR)
    ?p <- (pieza origen ?o destino ?x)
    ?b <- (box ?x piezas ?pB destino ?d entregadas ?e)
    (test (> ?pR 0))
=>
    (retract ?r)
    (retract ?p)
    (retract ?b)
    (assert (robot boxAcual ?x piezas (- ?pR 1)))
    (assert (box ?x piezas ?pB destino ?d entregadas (+ ?e 1)))
)

(defrule moverDerecha
    (declare (salience 200))
    ?r <- (robot boxAcual ?x piezas ?pR)
    (test (< ?x ?*posMax*))
=>
;    (retract ?r)
    (assert (robot boxAcual (+ ?x 1) piezas ?pR))
)

(defrule moverIzquierda
    (declare (salience 150))
    ?r <- (robot boxAcual ?x piezas ?pR)
    (test (> ?x ?*posMin*))
=>
;    (retract ?r)
    (assert (robot boxAcual (- ?x 1) piezas ?pR))
)

(defrule fin
    (declare (salience 50))
    (robot boxAcual ?x piezas 0)
    (box 1 piezas 0 destino ?d entregadas ?e)
    (box 2 piezas 0 destino ?d entregadas ?e)
    (box 3 piezas 0 destino ?d entregadas ?e)
    (box 4 piezas 0 destino ?d entregadas ?e)
    (box 5 piezas 0 destino ?d entregadas ?e)

=>
    (printout t "Todas las piezas han sido entregadas!!" crlf)
    (halt)
)